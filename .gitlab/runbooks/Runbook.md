# Squid-Proxy Helm Chart Runbook

This runbook provides operational procedures and troubleshooting steps for the Squid Proxy Helm Chart.

## Monitoring

Monitor the status of the deployed DaemonSet using `kubectl get ds`.

## Updating

Update the Helm Chart using `helm upgrade`.

## Troubleshooting

1. Check the logs of the Squid proxy pods using `kubectl logs`.
2. Check the events of the Squid proxy pods using `kubectl describe`.
3. If the pods aren't starting, check the configured resource limits in the values.

## Adding Nodes

As DaemonSets automatically run the desired pods on new nodes that match the selection criteria, scaling is accomplished by adding new nodes to the cluster.

## Removing Nodes

If you want to remove a node from the DaemonSet, you can drain the node using `kubectl drain` before deleting it. Make sure to respect any disruption budgets you have specified.

## Contact

For any operational difficulties or questions, contact devops@ccsolutions.io.
