# Squid-Proxy Helm Chart

This Helm chart deploys a Squid proxy on a [Kubernetes](http://kubernetes.io) cluster using the [Helm](https://helm.sh) package manager.

## Prerequisites

- Kubernetes 1.12+
- Helm 3.0+

## Installing the Chart

To install the chart with the release name `my-release`:

```bash
$ helm install my-release path_to_chart_directory/squid-proxy
```

## Configuration

The following table lists the configurable parameters of the Squid Proxy chart and their default values.

| Parameter                    | Description | Type | Default |
|------------------------------| --- | --- | --- |
| `image.repository`           | Repository des Squid-Proxy-Images | String | `nginx` |
| `image.pullPolicy`           | Pull Policy des Squid-Proxy-Images | String | `IfNotPresent` |
| `image.tag`                  | Tag des Squid-Proxy-Images | String | `""` |
| `imagePullSecrets`           | Image Pull Secrets | List | `[]` |
| `nameOverride`               | Überschreibt den Namen des Charts | String | `""` |
| `fullnameOverride`           | Überschreibt den vollständigen Namen des Charts | String | `""` |
| `serviceAccount.create`      | Legt fest, ob ein Servicekonto erstellt werden soll | Boolean | `true` |
| `serviceAccount.annotations` | Annotationen zum Hinzufügen zum Servicekonto | Object | `{}` |
| `serviceAccount.name`        | Der Name des zu verwendenden Servicekontos | String | `""` |
| `podAnnotations`             | Annotationen zum Hinzufügen zu den Pods | Object | `{}` |
| `podSecurityContext`         | Sicherheitskontexteinstellungen für Pods | Object | `{}` |
| `securityContext`            | Sicherheitskontexteinstellungen für Container | Object | `{}` |
| `service.type`               | Typ des Kubernetes-Dienstes | String | `ClusterIP` |
| `service.port`               | Port des Kubernetes-Dienstes | Integer | `80` |
| `ingress.enabled`            | Aktiviert/Deaktiviert den Ingress | Boolean | `false` |
| `ingress.className`          | Ingress-Klassenname | String | `nginx` |
| `ingress.annotations`        | Annotationen zum Hinzufügen zum Ingress | Object | `{}` |
| `ingress.hosts`              | Liste der Ingress-Hosts | List | `[chart-example.local]` |
| `ingress.tls`                | TLS-Konfiguration für den Ingress | List | `[]` |
| `squidConfig.enabled`        | Aktiviert/Deaktiviert die Squid-Konfiguration | Boolean | `false` |
| `squidConfig.conf`           | Squid-Konfiguration | String | `""` |
| `squidConfig.snippet`        | Squid-Konfigurationsschnipsel | String | `""` |
| `resources`                  | Ressourcenanforderungen und -limits für den Container | Object | `{}` |
| `nodeSelector`               | Node-Selektor-Tags | Object | `{}` |
| `tolerations`                | Tolerations für die Pod-Zuweisung | List | `[]` |
| `affinity`                   | Affinitätsregeln für die Pod-Zuweisung | Object | `{}` |
| `metrics.enabled`            | Aktiviert/Deaktiviert den Squid-Exporter | Boolean | `false` |
| `metrics.image.repository`   | Repository des Squid-Exporter-Images | String | `boynux/squid-exporter` |
| `metrics.image.tag`          | Tag des Squid-Exporter-Images | String | `latest` |
| `metrics.image.pullPolicy`   | Pull Policy des Squid-Exporter-Images | String | `IfNotPresent` |
| `metrics.port`               | Port des Squid-Exporters | Integer | `9301` |
| `metrics.resources`          | Ressourcen des Squid-Exporters | Object | `{}` |

## Contact

Maintained by CCSolution.io UG. You can reach us at devops@ccsolutions.io for any queries.


